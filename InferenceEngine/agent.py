import re

INPUT_FILE = "input6.txt"
QUERY = ""
KB_size = 0
CLAUSES = []
KB_CONSTANTS = []

def read_file():
    # Read the input file and extracts query, KB size & KB clauses.
    # Sample input is as follows:
    ## Diagnosis(John,Infected) -- Query
    ## 6 -- KB Size 
    ## HasSymptom(x,Diarrhea)=>LostWeight(x)
    ## LostWeight(x)&Diagnosis(x,LikelyInfected)=>Diagnosis(x,Infected)
    ## HasTraveled(x,Tiberia)&HasFever(x)=>Diagnosis(x,LikelyInfected)
    ## HasTraveled(John,Tiberia)
    ## HasFever(John)
    ## HasSymptom(John,Diarrhea)
    try:
        file = open(INPUT_FILE, "r")
    except IOError:
        print ("Error reading the file")
        exit()
    file_content = [line.strip() for line in file.readlines()]
    file.close()
    global QUERY, KB_size, CLAUSES
    QUERY = file_content[0]
    KB_size = int(file_content[1])
    CLAUSES = file_content[2:]
    print (QUERY)
    print (KB_size)
    print (CLAUSES)

def inference():
    # Do the backward chaining to infer the query from KB.

    global QUERY, KB_size, CLAUSES, KB_CONSTANTS
    
    # Build map with clause RHS as a dict key.
    clause_map = {} # Mapping from RHS to LHS.
    for c in CLAUSES:
        if "=>" in c: # Clause of form a[^b^c..] => x
            clause_contents = c.split("=>");
            # Collect the list of constants from each term of the clause.
            KB_CONSTANTS.extend(get_clause_constants(clause_contents[0]))
            KB_CONSTANTS.extend(get_clause_constants(clause_contents[1]))
            # Handle multiple clauses with same consequent.
            if clause_contents[1] in clause_map:
                clause_map[clause_contents[1]].append(clause_contents[0])
            else:
                clause_map[clause_contents[1]] = [clause_contents[0]];
        else: # Atomic clause.
            KB_CONSTANTS.extend(get_clause_constants(c))
            clause_map[c] = [1];
            
    KB_CONSTANTS = list(set(KB_CONSTANTS))
    print (KB_CONSTANTS)
    print (clause_map)
    #exit()
    
    # Open a output file for writing the Inference result.
    op_file = open("output.txt", "w")
    #QUERY = "HasFever(John)" # Test case.
    # If the QUERY is present as a fact, then all set! :)
    if QUERY in clause_map and clause_map[QUERY][0] == 1:
        op_file.write("TRUE")
        op_file.close()
        return
    
    result = backward_chaining(QUERY,clause_map)
    print ("Result = ", result)
    if (result):
        op_file.write("TRUE")
    else:
        op_file.write("FALSE")
    op_file.close()
        
    
def backward_chaining(query,clause_map):
    print ("Backward chaining for ", query)
    # Runs the Backward chaining inference for a given query
    # on the knowledge base.
    if query in clause_map and clause_map[query][0] == 1:
        return True
    
    for (k,vlist) in clause_map.items():
        for v in vlist:
            (lhs, rhs_list) = unify(k,v, query)
            # XXX: Here LHS is the consequent and RHS the antecedent of KB.
            # Go to next clause in case of failure.
            if (lhs == 0 and rhs_list[0] == 0): continue
            print (rhs_list)
            #print (lhs, "->" , rhs , "----" , query)
            # If the unification is successful,
            # then do backward chaining on the antecedents.
            is_rhs_true = True
            for rhs in rhs_list:
                print ("(",k,",",v, ") |||||",lhs, "->" , rhs , "----" , query)
                is_rhs_true = True
                for a in rhs.split("&"):
                    is_rhs_true = is_rhs_true and backward_chaining(a, clause_map)
                if is_rhs_true:
                    return True
            
    return False
    
def unify(k, v, query):
    # Unifies the clause with the query.
    print (query, "~~", k, "~~",v)
    # If query is same as the k (consequent) of the clause,
    # check for presence of variable 'x' in v (antecedent)
    # and unify accordingly.
    if k == query:
        # Query: Q(C1)
        # Clause: A(x)^B(x)=>Q(C1)
        # For above case, unify with each available constant and infer using
        # backward chaining for each unification by constant.
        if re.search ('(\(|,)x(,|\))', v):
            global KB_CONSTANTS
            val = []
            for c in KB_CONSTANTS:
                val.append(re.sub('(\(|,)x(,|\))', '\\1'+ c + '\\2', v))
            return (k, val)

    if k != query:
        # Split the name and args of predicate and compare the names for
        # going further with unification.
        key_parts = k.split("(")
        query_parts = query.split("(")
        # Return failure if the predicate names are not same.
        if len(key_parts) != len(query_parts) or key_parts[0] != query_parts[0]:
            return (0,[0]) # Failure
        # Compare the args of the predicate and
        # substitute 'x' with the right argument.
        key_args = key_parts[1].replace(")", "")
        key_args = key_args.split(",")
        query_args = query_parts[1].replace(")", "")
        query_args = query_args.split(",")
        # Get the right argument to substitute for 'x'.
        substitution = ""
        for (a,b) in zip(key_args, query_args):
            if (a == b):
                continue
            elif (a == "x" and (substitution == ""
                    or substitution == b)):
                substitution = b
            else:
                return (0, [0])
        new_key = re.sub('(\(|,)x(,|\))', '\\1'+ substitution + '\\2', k)
        new_val = re.sub('(\(|,)x(,|\))', '\\1'+ substitution + '\\2', v)
        
    return (new_key, [new_val])

def get_clause_constants(term):
    # Extracts the constants from the term and returns it as a list.
    constants = []
    # Complex term of form A(P,Q)^B(M,N)
    terms = term.split ("^")
    for t in terms:
        for arg_str in re.findall('\((.*?)\)', t):
            print (arg_str)
            #args_list = arg_str.split(",")
            for a in arg_str.split(","):
                if a != 'x':
                    constants.append(a)
    return constants

def main():
    read_file()
    inference()
    
main()