/**
Author: Dharmik kumar Locharam
Date: 20140924
CSCI 561 Artificial Intelligence Assignment 1
Task: Implement BFS, DFS and UCS for the given graph
- Reads input from input.txt and outputs the log, path and cost to output.txt
**/

#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <map>
#include <stack>
#include <algorithm>
#include <functional>
#define MAX 1000
using namespace std;

// Node structure for UCS queue as we need both name and path cost in heap.
struct ucsnode {
    string name;
    string parent;
    int cost;
};

struct comparator{
    bool operator() (ucsnode const& i, ucsnode const& j){
        if (i.cost == j.cost) {
            return i.name.compare(j.name) > 0;
        }
        return i.cost > j.cost;
    }
};

// Input vars
int run_mode, nodes = 0;
string source, destination, names[MAX], namesSorted[MAX];
int weights[MAX][MAX];
map<string, int> nameIndexMap;
string SPL = "########";

// Open file for printing output.
ofstream fout ("output.txt");

void get_inputs() {
    ifstream fin("input.txt");
    if (!fin.is_open()) {
        fout << "Cannot open input file. Please check if the file is present."<< endl;
        fout.close();
    }
    fin >> run_mode >> source >> destination>>nodes;
    for (int i = 0; i < nodes; i++) {
        //cin>>names[i];
        fin>>names[i];
        namesSorted[i] = names[i];
        nameIndexMap[names[i]] = i;
    }

    for (int i = 0; i < nodes; i++) {
        for (int j = 0; j < nodes; j++) {
            //cin >> weights[i][j];
            fin >> weights[i][j];
        }
    }
    // Done with reading inputs.Close the file.
    fin.close();
    sort(namesSorted, namesSorted + nodes);
}

void print_inputs() {
    cout <<run_mode<<endl<<source<<endl<<destination<<endl << nodes<< endl;
    for (int i = 0; i < nodes; i++) {
        cout << names[i]<<"--"<< namesSorted[i] << endl;
    }

    for (int i = 0; i < nodes; i++) {
        for (int j = 0; j < nodes; j++) {
            cout << weights[i][j] << " ";
        }
        cout << endl;
    }
}

void print_log(string log[], int log_counter)
{
    int i = 0;
    //fout << "LOG: ";
    for (; i < log_counter - 1; i++) {
        fout << log[i] << "-";
    }
    fout << log[i] <<  endl;
}

void print_path_cost(map<string, string> parent, string node)
{
    string path[MAX];
    int counter = 0;
    int cost = 0;
    path[counter++] = node;
    while(parent[node].compare("") != 0) {
        cost += weights[nameIndexMap[parent[node]]][nameIndexMap[node]];
        node = parent[node];
        path[counter++] = node;
    }
    //cout << "PATH: ";
    for (int i = counter - 1; i > 0 ;i--) {
        fout << path[i] << "-";
    }
    fout << path[0] <<endl;
    // Print cost.
    fout << cost << endl;
}

void print_no_path() {
    fout << "NoPathAvailable" << endl;
}

string get_least_node(vector<string>& v) {
    string min = v.front();
    /*if(min.compare(SPL) == 0){
        cout << "Special charater at the front of queue is not popped. Please check" << endl;
    }*/
    int counter = -1;
    int min_counter = 0;
    for(vector<string>::iterator it = v.begin(); it != v.end(); ++it) {
        if ((*it).compare(SPL) == 0) {
            break;
        }
        counter++;
        if((*it).compare(min) < 0) {
            min = *it;
            min_counter = counter;
        }
    }
    /*cout<< " Before deleting" << endl;
    for(vector<string>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << (*it) << "-";
    }
    cout << endl;
    cout << "After deleting from pos " << min_counter<<endl;*/
    // Remove the min element from the list and return.
    v.erase(v.begin() + min_counter);
    
    /*for(vector<string>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << (*it) << "-";
    }
    cout << endl;*/
     return min;
}

void bfs_new() {
    // Create a queue for storing child nodes.
    vector<string> bfsQ;
    
    // BFS Initialization.
    int explored[MAX], depth[MAX];
    int depth_counter = 0;
    for (int i = 0; i < MAX; i++) {
        explored[i] = 0;
        depth[i] = -1;
    }
    map<string, string> parent;
    string log[MAX]; // Stores the log of the nodes visited in order.
    // source node initialization
    parent[source] = "";
    explored[nameIndexMap[source]] = 1;
    depth[nameIndexMap[source]] = 0;
    bfsQ.push_back(source);
    bfsQ.push_back(SPL);

    int log_counter = 0;
    // Start the BFS.
    while(!bfsQ.empty()) {
        string node = bfsQ.front();
        // If the front node is spl char, then increment depth level, insert spl char at the
        // end and continue.
        if (node.compare(SPL) == 0) {
            depth_counter += 1;
            //cout << "popping special char" << endl;
            bfsQ.erase(bfsQ.begin());
            // Push the special char to separate level only if there are nodes after this char.
            if(bfsQ.size() > 0) {
            	bfsQ.push_back(SPL);
            }
            continue;
        }
        // else part where the queue has the non special character.
        // Get the node with least lexicon order.
        node = get_least_node(bfsQ);
        //string node = bfsQ.front();
        //bfsQ.pop();

        log[log_counter++] = node;
        // If the popped node is a destination node, work is done!
        if (node.compare(destination) == 0) {
            print_log(log, log_counter);
            print_path_cost(parent, node);
            return;
        }
        //Expand the node.
        int parent_index = nameIndexMap[node];
        for (int i = 0; i < nodes; i++) {
            int child_index = nameIndexMap[namesSorted[i]];
            if (explored[child_index] != 1 && parent_index != child_index
                && weights[parent_index][child_index] > 0) {
                //cout << namesSorted[i]<<endl;
                explored[nameIndexMap[namesSorted[i]]] = 1;
                parent[namesSorted[i]] = node;
                depth[nameIndexMap[namesSorted[i]]] = depth_counter;
                bfsQ.push_back(namesSorted[i]);
            }
        }
		//explored[nameIndexMap[node]] = 1;
    }
    // If the execution reaches here, then there is no path.
    //print_log(log, log_counter);
    print_no_path();
}

string get_least_node_dfs(vector<string> &v) {
    string min = v.back();
    /*if (min.compare(SPL) == 0) {
        cout << "Special charater at the front of queue is not popped. Please check" << endl;
    }*/
    int min_counter = v.size();
    for(int i = v.size() - 1; i >= 0; i--) {
        if (v[i].compare(SPL) == 0) {
            break;
        }
        if (v[i].compare(min) < 0) {
            min = v[i];
            min_counter = i;
        }
    }
    /*cout<< " Before deleting" << endl;
    for(vector<string>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << (*it) << "-";
    }
    cout << endl;
    cout << "After deleting from pos " << min_counter<<endl;*/
    // Remove the min element from the list and return.
    v.erase(v.begin() + min_counter);
    /*for(vector<string>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << (*it) << "-";
    }
    cout << endl;*/
     return min;
}

void dfs_new() {
    // Create a stack to carry out DFS.
    vector<string> dfsStack;
    // DFS Initialization.
    int explored[MAX], depth[MAX], visited[MAX], depth_counter = 0;
    for (int i = 0; i < MAX; i++) {
        explored[i] = 0;
        depth[i] = -1;
        visited[i] = 0;
    }
    map<string, string> parent;
    string log[MAX]; // Stores the log of the nodes visited in order.
    // source node initialization
    parent[source] = "";
    visited[nameIndexMap[source]] = 1;
    depth[nameIndexMap[source]] = 0;
    dfsStack.push_back(SPL);
    dfsStack.push_back(source);

    int log_counter = 0;
    //Start DFS.
    while(!dfsStack.empty()) {
        string node = dfsStack.back();
        // If its a special character, then just pop and continue.
        if(node.compare(SPL) == 0) {
            dfsStack.pop_back();
            //depth_counter--;
            continue;
        }
        // Get the node which has smallest lexicon order.
        node = get_least_node_dfs(dfsStack);
        //dfsStack.pop();
        log[log_counter++] = node;
        explored[nameIndexMap[node]] = 1;
       
        // If the popped node is a destination node, work is done!
        if (node.compare(destination) == 0) {
            print_log(log, log_counter);
            print_path_cost(parent, node);
            return;
        }
        // Expand the node.
        // Push the spl character for depth separation.
        dfsStack.push_back(SPL);
        // Push the child nodes on to the queue in reverse order.
        int parent_index = nameIndexMap[node];
        int parent_depth = depth[parent_index];
        for (int i = nodes - 1; i >= 0 ; i--) {
            int child_index = nameIndexMap[namesSorted[i]];
            if (parent_index != child_index && weights[parent_index][child_index] > 0
                && (depth[child_index] == -1 || depth[child_index] > parent_depth + 1)) {
                parent[namesSorted[i]] = node;
                depth[child_index] = depth[parent_index] + 1;
                dfsStack.push_back(namesSorted[i]);
            }
        }
        dfsStack.push_back(SPL);
        //depth_counter++;
        //explored[nameIndexMap[node]] = 1;
    }
    // If the execution reaches here, then there is no path.
    //print_log(log, log_counter);
    print_no_path();
}

void print_vector(vector<ucsnode>& v) {
	for(vector<ucsnode>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << (*it).name << "(" << (*it).cost << ")"<< "||";
    }
    cout << endl;
}

ucsnode my_pop_heap(vector<ucsnode>& v) {
    pop_heap(v.begin(), v.end(), comparator());
    ucsnode n = v.back();
    v.pop_back();
    //cout << "Aftering popping " << n.name << " >>";
    //print_vector(v);
    return n;
}

void my_push_heap(vector<ucsnode>& v, ucsnode n) {
    v.push_back(n);
    push_heap(v.begin(), v.end(), comparator());
    //cout << "After pushing " << n.name << " >>";
    //print_vector(v);
}

void ucs() {
    int explored[MAX];
    for (int i = 0; i < MAX; i++) {
        explored[i] = 0;
    }
    map<string, string> parent;
    string log[MAX];
    int log_counter = 0;
    // Create a vector with cost.
    int ucs_cost[MAX];
    for  (int i = 0; i < MAX; i++) {
        ucs_cost[i] = -1;
    }
    // Initialize a vector which is used for heap operations.
    vector<ucsnode> ucsQ;
    // Create a source node and push it to UCS Queue.
    ucsnode s;
    s.cost = 0;
    ucs_cost[nameIndexMap[source]] = 0;
    s.name= source;
    s.parent = "";
    ucsQ.push_back(s);
    make_heap (ucsQ.begin(),ucsQ.end(), comparator());
    while(ucsQ.size() != 0) {
        ucsnode node = my_pop_heap (ucsQ);
        //cout<<"Popped node = "<<node.name <<"("<<node.cost<<")"<<endl;
        // If the popped node is already explored with lesser cost, dont consider it.
        if (ucs_cost[nameIndexMap[node.name]] != -1 && 
        	ucs_cost[nameIndexMap[node.name]] < node.cost) {
        	//cout << node.name << " is already explored with lesser cost" << endl;
        	continue;
        }
        log[log_counter++] = node.name;
        explored[nameIndexMap[node.name]] = 1;
        parent[node.name] = node.parent;
        // If the popped node is a destination node, work is done!
        if (node.name.compare(destination) == 0) {
            //parent[node.name] = node.parent;
            print_log(log, log_counter);
            print_path_cost(parent, destination);
            return;
        }

        // Push the child nodes on to the heap.
        for (int i = 0; i < nodes ; i++) {
            int parent_index = nameIndexMap[node.name];
            int child_index = nameIndexMap[namesSorted[i]];
            int edge_cost = weights[parent_index][child_index];
            if (parent_index != child_index && weights[parent_index][child_index] != 0
                && (ucs_cost[child_index] == -1 ||
                        ucs_cost[child_index] > edge_cost + node.cost)) {
                //parent[namesSorted[i]] = node.name;
                ucsnode new_node;
                new_node.name = namesSorted[i];
                new_node.cost = edge_cost + node.cost;
                ucs_cost[child_index] = new_node.cost;
                new_node.parent= node.name;
                my_push_heap(ucsQ, new_node);
            }
        }
    }
    // If the execution reaches here, then there is no path.
    //print_log(log, log_counter);
    print_no_path();
}

int main() {
    get_inputs();
    // Run different searches depending on input.
    if (run_mode == 1) {
       bfs_new();
    } else if (run_mode == 2) {
        dfs_new();
    } else if (run_mode == 3) {
        ucs();
    }
    fout.close();
    return 0;
}
